package contest;

import java.io.*;
import java.util.*;

//-----------------------------------------------------
public class recursiveStringReverse 
{
	//-----------------------------------------------------
	public static void main(String[] args) throws IOException 
	{
		String token=readInput();
		String output=process(token);
		System.out.println(output);
	}
	//-----------------------------------------------------
	static String process(String token)
	{
		if (token.length()==1)
			return token;
		else if (token.length()==2)
			return token.charAt(1)+""+token.charAt(0);
		else
			return token.charAt(token.length()-1)+process(token.substring(1,token.length()-1))+token.charAt(0);
	}
	//-----------------------------------------------------
	static String readInput() throws IOException
	{
		StreamTokenizer streamTokenizer = new StreamTokenizer(new BufferedReader(new InputStreamReader(System.in)));
		int count=0;
		int row=0;
		int col=0;
		int[] tokens=new int[3];
		String string="";
		while ((streamTokenizer.nextToken()) != StreamTokenizer.TT_EOF) 
		{
			string= streamTokenizer.sval;
			break;
		}
		return string;
	}
	//-----------------------------------------------------
}
//-----------------------------------------------------











