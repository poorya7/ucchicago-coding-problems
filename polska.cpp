#include <iostream>
#include <cstdlib>
#include <vector>
#include <string>
#include <sstream>
using namespace std;
//-----------------------------------------------------
string doOperation(string a, string b, string operand);
bool is_number(const std::string& s);
std::vector<string> readInput();
string toString(int number);
bool toBool(std::string str);
string process(std::vector<string> tokens);
//-----------------------------------------------------
int main()
{
     std::vector<string> vect=readInput();
     string output=process(vect);
     cout<<output;

}
//-----------------------------------------------------
string process(std::vector<string> tokens)
{
   std::vector<string> stack;
    for (int i=0; i<tokens.size(); i++)
    {
        string token=tokens.at(i);
        if (token=="+" || token=="*" || token=="==" || token=="and" || token=="or")
        {
            //OPERATION
            if (stack.size()>=2)
            {
                string a=stack.back();
                stack.pop_back();
                string b=stack.back();
                stack.pop_back();
                stack.push_back(doOperation(a,b,token));
            }
            else
                return "SYNTAX ERROR";
        }
        else
            stack.push_back(token);
    }
    if (stack.size()>1)
        return "SYNTAX ERROR";
    else
    {
        string s=stack.back();
        stack.pop_back();
        return s;
    }
}
//-----------------------------------------------------
 string doOperation(string a, string b, string operand)
{
    bool cond1=is_number(a) && !is_number(b);
    bool cond2=is_number(b) && !is_number(a);

    bool cond3=!is_number(a) || !is_number(b);
    bool cond4=operand=="*" || operand=="+";

    bool cond5=is_number(a) || is_number(b);
    bool cond6=operand=="and" || operand=="or";

    if (cond1 || cond2 || (cond3 && cond4) || (cond5 && cond6))
        return "TYPE ERROR";

    if (operand=="*")
    {
       int aa= atoi(a.c_str());
       int bb= atoi(b.c_str());
       return toString(aa*bb);
    }

    else if (operand=="+")
    {
        int aa= atoi(a.c_str());
        int bb= atoi(b.c_str());
        return toString(aa+bb);
    }
    else if (operand=="==")
    {
        if ((a==b)==1)
            return "true";
        else
            return "false";
    }
    else if (operand=="and")
    {
         string s=toString(toBool(a) && toBool(b));
         if (s=="1")
            return "true";
        else
            return "false";
    }
    else if (operand=="or")
    {
        string s=toString(toBool(a) || toBool(b));
        if (s=="1")
            return "true";
        else
            return "false";
    }
    else
        return "";
}
//-----------------------------------------------------
bool toBool(std::string str)
{
    if (str=="true")
        return true;
    else
        return false;
}
//-----------------------------------------------------
string toString(int number)
{
    stringstream ss;
    ss << number;
    return ss.str();
}
//-----------------------------------------------------
bool is_number(const std::string& s)
{
    std::string::const_iterator it = s.begin();
    while (it != s.end() && std::isdigit(*it)) ++it;
    return !s.empty() && it == s.end();
}
//-----------------------------------------------------
 std::vector<string> readInput()
{
    string input;
    std::getline (std::cin,input);

    std::vector<string> vect;

    std::stringstream ss(input);

    string i;

    while (ss >> i)
    {
        vect.push_back(i);

        if (ss.peek() == ' ')
            ss.ignore();
    }
    return vect;
}
//-----------------------------------------------------

//-----------------------------------------------------
