import java.io.*;
import java.util.*;

//-----------------------------------------------------
public class GreedyHike 
{
	static int startx;
	static int starty;
	//-----------------------------------------------------
	public static void main(String[] args) throws IOException 
	{
		List<Integer> tokens=readInput();
		int[][] map=buildMap(tokens);
		String output=move(map);
		System.out.println(output);
	}
	//-----------------------------------------------------
	static String move(int[][] map)
	{
		int x=startx;
		int y=starty;
		int current=map[x][y];
		int sum=0;
		while (y<map[0].length-1)
		{
			y++;
			int diff1=Integer.MAX_VALUE;
			int diff2=Integer.MAX_VALUE;
			int diff3=Integer.MAX_VALUE;
			if (x>0)
				diff1=Math.abs(map[x-1][y]-current);
			diff2=Math.abs(map[x][y]-current);
			if (x<map.length-1)
				diff3=Math.abs(map[x+1][y]-current);
			if (diff1<diff2 && diff1<diff3)
				x--;
			else if (diff3<diff2 && diff3<diff1)
				x++;
			else if (diff1==diff3)
				x=x++;
			
			sum+=Math.abs(current-map[x][y]);
			current=map[x][y];
		}
		String output=x+" "+y+" "+sum;
		return output;
	}
	//-----------------------------------------------------
	static void printMap(int[][] map)
	{
		for (int row=0; row<map.length; row++)
		{
			for (int col=0; col<map[0].length; col++)
				System.out.println(row+" "+col+" "+map[row][col]+" ");
		}
	}
	//-----------------------------------------------------
	static List<Integer> readInput() throws IOException
	{
		StreamTokenizer streamTokenizer = new StreamTokenizer(new BufferedReader(new InputStreamReader(System.in)));
		int count=0;
		int row=0;
		int col=0;
		List<Integer> tokens=new ArrayList<Integer>();
		while ((streamTokenizer.nextToken()) != StreamTokenizer.TT_EOF) 
		{
			count++;
			tokens.add((int) Math.round(streamTokenizer.nval));
			if (count==1)
				row= (int) Math.round(streamTokenizer.nval);
			else if (count==2)
				col=(int) Math.round(streamTokenizer.nval);
			else if (count==3)
				startx=(int) Math.round(streamTokenizer.nval);
			else if (count==4)
				starty=(int) Math.round(streamTokenizer.nval);
			else if (count==(row*col)+4)
				break;
		}
		return tokens;
	}
	//-----------------------------------------------------
	static int[][] buildMap(List<Integer> input)
	{
		int row=input.get(0);
		int col=input.get(1);
		int[][] map=new int[row][col];
		int index=4;
		for (int i=0; i<row; i++)
			for (int j=0; j<col; j++)
				map[i][j]=input.get(index++);
		return map;
	}
	//-----------------------------------------------------
}
//-----------------------------------------------------











