package contest;
import java.io.*;
import java.util.*;

//-----------------------------------------------------
public class apaxia 
{
	//-----------------------------------------------------
	public static void main(String[] args) throws IOException 
	{
		doApaxia();
	}
	//-----------------------------------------------------
	static void doApaxia() throws IOException
	{
		String input = new Scanner(System. in).nextLine();
		String[] tokens=input.split(" ");
		String lastname=tokens[1];
		int count=lastname.length();
		if (count==5)
			count=4;
		String output=cat(tokens[1], count);
		System.out.println(tokens[0]+" "+output);
	}
	//-----------------------------------------------------
	static String cat(String input, int count)
	{
		if (count==1)
			return input;
		else 
		{
			String str=cat(input, count/2);
			if (count%2==0)
				return str+str;
			else
				return str+input+str;
		}
	}
	//-----------------------------------------------------
}
//-----------------------------------------------------


