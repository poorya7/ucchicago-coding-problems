package contest;
import java.io.*;
import java.util.*;

//-----------------------------------------------------
public class polska 
{
	static String[] operations= {"+","*","==","and", "or"};
	//-----------------------------------------------------
	public static void main(String[] args) throws IOException 
	{
		String[] tokens=readInput();
		String output=process(tokens);
		System.out.println(output);
	}
	//-----------------------------------------------------
	static String process(String[] tokens)
	{
		Stack<String> stack =new Stack<String>();
		for (int i=0; i<tokens.length; i++)
		{
			String token=tokens[i];
			if (Arrays.asList(operations).contains(token))
			{
				//OPERATION
				if (stack.getSize()>=2)
				{
					String a=stack.pop();
					String b=stack.pop();
					stack.push(doOperation(a,b,token));
				}
				else return "SYNTAX ERROR";
			}
			else 
				stack.push(token);
		}
		if (stack.getSize()>1)
			return "SYNTAX ERROR";
		else
			return stack.pop();
	}
	//-----------------------------------------------------
	static String doOperation(String a, String b, String operand)
	{
		boolean cond1=isNumeric(a) && !isNumeric(b);
		boolean cond2=isNumeric(b) && !isNumeric(a);
		
		boolean cond3=!isNumeric(a) || !isNumeric(b);
		boolean cond4=operand.equals("*") || operand.equals("+");
		
		boolean cond5=isNumeric(a) || isNumeric(b);
		boolean cond6=operand.equals("and") || operand.equals("or");
		
		if (cond1 || cond2 || (cond3 && cond4) || (cond5 && cond6))
			return "TYPE ERROR";
		
		switch (operand) 
		{
		case "*":
			return Integer.toString(Integer.parseInt(a)*Integer.parseInt(b));
		case "+":
			return Integer.toString(Integer.parseInt(a)+Integer.parseInt(b));
		case "==":
			return Boolean.toString(Integer.parseInt(a)==Integer.parseInt(b));
		case "and":
			return Boolean.toString(Boolean.parseBoolean(a) && Boolean.parseBoolean(b));
		case "or":
			return Boolean.toString(Boolean.parseBoolean(a) || Boolean.parseBoolean(b));
		default:
			break;
		}
		return "";
	}
	//-----------------------------------------------------
	public static boolean isNumeric(String str)
	{
	    try
	    {
	      double d = Double.parseDouble(str);
	    }
	    catch(NumberFormatException nfe)
	    {
	      return false;
	    }
	    return true;
	}
	//-----------------------------------------------------
	static String[] readInput() throws IOException
	{
		Scanner scanner = new Scanner(System. in); 
		String input = scanner. nextLine();
		String[] tokens=input.split(" ");
		return tokens;
	}
	//-----------------------------------------------------
}
//-----------------------------------------------------


class Stack<T>
{
	List<T> values;
	public Stack()
	{
		values=new ArrayList<T>();
	}
	
	void push(T t)
	{
		values.add(t);
	}
	
	T pop()
	{
		T value=values.get(values.size()-1);
		values.remove(value);
		return value;
	}
	
	boolean isEmpty()
	{
		return values.size()==0;
	}
	
	int getSize()
	{
		return values.size();
	}
	
}







