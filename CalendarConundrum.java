import java.io.*;
import java.util.*;

//-----------------------------------------------------
public class CalendarConundrum 
{
	//-----------------------------------------------------
	public static void main(String[] args) throws IOException 
	{
		int[] tokens=readInput();
		String output=process(tokens);
		System.out.println(output);
	}
	//-----------------------------------------------------
	static String process(int[] tokens)
	{
		int a=tokens[0];
		int b=tokens[1];
		int c=tokens[2];
		String output="";
		if (a>31)
			output="Format #3";
		else if (a>12)
		{
			if (c>31)
				output="Format #2";
			else
				output="Ambiguous";
		}
		else
		{
			if (b>12)
				output="Format #1";
			else
				output="Ambiguous";	
		}
		return output;
	}
	//-----------------------------------------------------
	static int[] readInput() throws IOException
	{
		StreamTokenizer streamTokenizer = new StreamTokenizer(new BufferedReader(new InputStreamReader(System.in)));
		int count=0;
		int row=0;
		int col=0;
		int[] tokens=new int[3];
		while ((streamTokenizer.nextToken()) != StreamTokenizer.TT_EOF) 
		{
			int n=(int) Math.round(streamTokenizer.nval);
			tokens[count++]=n;
			if (count==3)
				break;
		}
		return tokens;
	}
	//-----------------------------------------------------
}
//-----------------------------------------------------











